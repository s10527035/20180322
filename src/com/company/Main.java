
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
public class FX extends Application
{ public static void main(String[] args) {
               launch(args);
}
   @Override
   public void start(Stage primaryStage) throws Exception {
              FlowPane flowPane = new FlowPane();
             // flowPane.setStyle("-fx-background-color: YELLOW");
                       TextField textField =new TextField();
               flowPane.getChildren().add(textField);
                Button btnOne =new Button("Click Me");
               btnOne.setOnAction(actionEvent -> {
                        Map<Integer,String> colors =new HashMap();
                        colors.put(1,"-fx-background-color: blue");
                       colors.put(2,"-fx-background-color: red");
                       colors.put(3,"-fx-background-color: black");

                                Random rand = new Random();
                        int n = rand.nextInt(3)+1;
                       System.out.println(rand.nextInt(3)+1);

                               flowPane.setStyle(colors.get(n));
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("HELLO,".concat(textField.getText()));
                        alert.show();
                    });


                               flowPane.getChildren().add(btnOne);
               // flowPane.setStyle("-fx-background-color: red");
                //       gridpane.getChildren().add(btnOne);
                                InnerShadow innerShadow = new InnerShadow();
               innerShadow.setColor(Color.RED);
                innerShadow.setOffsetX(5.0);
                innerShadow.setOffsetY(5.0);
                Text text = new Text("MY TEXT");
                text.setEffect(innerShadow);
                flowPane.getChildren().add(text);

                        Scene scene =new Scene(flowPane,300,300);

                        primaryStage.setTitle("TEST");
                primaryStage.setScene(scene);

                        setUserAgentStylesheet(STYLESHEET_CASPIAN);
                primaryStage.show();
            }
}

